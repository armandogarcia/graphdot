#!/usr/bin/env python
# -*- coding: utf-8 -*-
import warnings

warnings.warn(
    "The basekernel module has been relocated to graphdot.kernel.basekernel.\n"
    "Please update relevant import statements from\n"
    "'from graphdot.kernel.marginalized.basekernel import *'\n"
    "to\n"
    "'from graphdot.kernel.basekernel import *'"
)
