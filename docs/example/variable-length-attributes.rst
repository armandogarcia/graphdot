Graphs with Variable-Length Node and Edge Features
--------------------------------------------------------------------------------

.. literalinclude:: ../../example/variable-length-attributes.py
   :language: python
   :linenos:

Exptected output:

.. code-block:: none

   [[1.         0.35042228]
    [0.35042228 1.        ]]